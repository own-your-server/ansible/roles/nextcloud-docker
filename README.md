# Nextcloud docker

Nextcloud is an office cloud service with all necessary tools like agenda, documents, online edition,...

## Installation

### dependencies:

needed:
* [ansible docker host](https://framagit.org/own-your-server/ansible/roles/docker-host.git)
    * at least having docker installed with a network called web

* [ansible traefik docker](https://framagit.org/own-your-server/ansible/roles/traefik-docker.git)
    * at least having a traefik reverse proxy configured to listen to web

* [ansible fail2ban](https://framagit.org/own-your-server/ansible/roles/fail2ban.git)

### Variables
All variables are under `defaults/main.yml`

all variable that doesn't have default value need to be overided. You can also overide other varibles depending your need.
